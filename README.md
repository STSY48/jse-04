# Task-manager
Учебный проект в рамках изучения Java

# Требования к software
- Java Openjdk v11
- Apache maven 3.6.1

# Разработчик
Складчиков Игорь
skladchikov_iy@nlmk.com

# Команды
### Сборка
```
mvn package
```
### Запуск
```
java -jar target\task-manager-1.0.0.jar
```

# Поддерживаемые терминальные команды
```
help - вывод списка команд
version - информация о версии приложения
about - информация о разработчике
exit - выход из терминального приложения
```